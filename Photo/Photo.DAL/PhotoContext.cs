﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Photo.DAL.Entity;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions; 

namespace Photo.DAL
{
    public class PhotoContext: DbContext 
    {
        public PhotoContext()
            : base("PhotoConnection")
        {
        }

        public DbSet<Admin> Admins { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Password> Passwords { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Follow> Followers { get; set; }
        public DbSet<Picture> Pictures { get; set; }
        public DbSet<Subscribe> Subscribes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        } 
    }
}
