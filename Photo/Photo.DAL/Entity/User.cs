﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Photo.DAL.Entity
{
    public class User: Entity
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string e_mail { get; set; }
        public byte[] photo { get; set; }

        public virtual Password Pas { get; set; }
        public virtual Admin Admin { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Picture> Pictures { get; set; }
        public virtual ICollection<Follow> Followers { get; set; }
        public virtual ICollection<Subscribe> Subscribes { get; set; } 
    }
}
