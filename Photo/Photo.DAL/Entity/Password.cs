﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Photo.DAL.Entity
{
    public class Password: Entity
    {
        public Int32 UserID { get; set; }
        public string Password { get; set; }

        public virtual User User { get; set; }
    }
}
