﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Photo.DAL.Entity
{
    public class Comment: Entity
    {
        public Int32 UserID { get; set; }
        public Int32 PicID { get; set; }
        public string Text { get; set; }
        public string Date { get; set; }

        public virtual User User { get; set; }
        public virtual Picture Pic { get; set; }
    }
}
