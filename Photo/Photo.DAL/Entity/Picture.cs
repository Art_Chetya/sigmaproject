﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Photo.DAL.Entity
{
    public class Picture: Entity
    {
        public Int32 UserID { get; set; }
        public byte[] Pic { get; set; }
        public string Date { get; set; }

        public virtual User User { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
    }
}
